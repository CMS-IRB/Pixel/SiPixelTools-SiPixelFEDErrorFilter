import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing

###############################
####### Parameters ############
###############################

options = VarParsing ('python')

options.register('reportEvery', 100,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.int,
    "Report every N events (default is N=100)"
)
options.register('wantSummary', True,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Print out trigger and timing summary"
)

## changing default values for arguments already registered by the Framework
options.setDefault('maxEvents', -1)

options.parseArguments()

from Configuration.Eras.Era_Run3_pp_on_PbPb_2023_cff import Run3_pp_on_PbPb_2023

process = cms.Process("USER", Run3_pp_on_PbPb_2023)

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = options.reportEvery

## For loading geometry and calibrations
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_Data_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '132X_dataRun3_Express_v4', '')

## Events to process
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )

## Input files
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        # FED error type 36 appears in FED 1326 in Run 375202, Event 158786571, LumiSection 177 in the following file
        'root://eoscms.cern.ch//eos/cms/store/express/HIRun2023A/HIExpressPhysics/FEVT/Express-v2/000/375/202/00000/30b097a9-ce34-42bb-9560-b59c65dddf37.root'
    )
)

# Full list for /HIExpressPhysics/HIRun2023A-Express-v2/FEVT and Run 375202
#with open('HIExpressPhysics_HIRun2023A-Express-v2_FEVT__Run_375202.txt', 'r') as file_list:
    #process.source.fileNames.extend(file_list.read().splitlines())

## Options and Output Report
process.options   = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(options.wantSummary),
    allowUnscheduled = cms.untracked.bool(False)
)

## Adapt SiPixelRawToDigi to HI input
process.siPixelDigis.cpu.InputLabel = cms.InputTag('rawDataRepacker')

## Load SiPixelFEDErrorFilter
process.siPixelFEDErrorFilter = cms.EDFilter("SiPixelFEDErrorFilter",
    src = cms.InputTag("siPixelDigis"),
    errorTypes = cms.vint32(36)
)

## Output file
process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('HIExpressPhysics_FEDError36.root'),
    outputCommands = cms.untracked.vstring(
        "keep *"
    ),
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('p')
    )
)

## Let it run
process.p = cms.Path(process.siPixelDigis*process.siPixelFEDErrorFilter)

process.outpath = cms.EndPath(process.out)
