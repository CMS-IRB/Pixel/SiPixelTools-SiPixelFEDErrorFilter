// -*- C++ -*-
//
// Package:    SiPixelTools/SiPixelFEDErrorFilter
// Class:      SiPixelFEDErrorFilter
//
/**\class SiPixelFEDErrorFilter SiPixelFEDErrorFilter.cc SiPixelTools/SiPixelFEDErrorFilter/plugins/SiPixelFEDErrorFilter.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Dinko Ferencek
//         Created:  Mon, 16 Oct 2023 13:02:33 GMT
//
//

// system include files
#include <memory>
#include <iostream>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDFilter.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"

#include "DataFormats/Common/interface/DetSetVector.h"
#include "DataFormats/SiPixelRawData/interface/SiPixelRawDataError.h"

//
// class declaration
//

class SiPixelFEDErrorFilter : public edm::stream::EDFilter<> {
public:
  explicit SiPixelFEDErrorFilter(const edm::ParameterSet&);
  ~SiPixelFEDErrorFilter() override;

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginStream(edm::StreamID) override;
  bool filter(edm::Event&, const edm::EventSetup&) override;
  void endStream() override;

  //void beginRun(edm::Run const&, edm::EventSetup const&) override;
  //void endRun(edm::Run const&, edm::EventSetup const&) override;
  //void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
  //void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

  // ----------member data ---------------------------
  const edm::InputTag src_;
  const edm::EDGetTokenT<edm::DetSetVector<SiPixelRawDataError>> token_;
  const std::vector<int> errorTypes_;

};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
SiPixelFEDErrorFilter::SiPixelFEDErrorFilter(const edm::ParameterSet& iConfig)
  //now do what ever initialization is needed
  :
  src_(iConfig.getParameter<edm::InputTag>("src")),
  token_(consumes<edm::DetSetVector<SiPixelRawDataError>>(src_)),
  errorTypes_(iConfig.getParameter<std::vector<int>>("errorTypes"))
  
{
}

SiPixelFEDErrorFilter::~SiPixelFEDErrorFilter() {
  // do anything here that needs to be done at destruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called on each new Event  ------------
bool SiPixelFEDErrorFilter::filter(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  
  edm::Handle<edm::DetSetVector<SiPixelRawDataError>> fedErrors;
  iEvent.getByToken(token_, fedErrors);
  if (!fedErrors.isValid()) {
    edm::LogError("SiPixelFEDErrorFilter") << "SiPixelRawDataErrors collection (" << src_.encode()
                                           << ") not found \n";
    return false;
  }

  for (auto it = fedErrors->begin(); it != fedErrors->end(); ++it) {
    for (auto& siPixelRawDataError : *it) {
      int type = siPixelRawDataError.getType();
      
      for(auto const& error: errorTypes_) {
        if (type == error) {
          edm::LogWarning("SiPixelFEDErrorFilter") << "Found error type " << type << " in FED " << siPixelRawDataError.getFedId();
          return true;
        }
      }
    }
  }
  
  return false;
}

// ------------ method called once each stream before processing any runs, lumis or events  ------------
void SiPixelFEDErrorFilter::beginStream(edm::StreamID) {
  // please remove this method if not needed
}

// ------------ method called once each stream after processing all runs, lumis and events  ------------
void SiPixelFEDErrorFilter::endStream() {
  // please remove this method if not needed
}

// ------------ method called when starting to processes a run  ------------
/*
void
SiPixelFEDErrorFilter::beginRun(edm::Run const&, edm::EventSetup const&)
{ 
}
*/

// ------------ method called when ending the processing of a run  ------------
/*
void
SiPixelFEDErrorFilter::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
void
SiPixelFEDErrorFilter::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
void
SiPixelFEDErrorFilter::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void SiPixelFEDErrorFilter::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}
//define this as a plug-in
DEFINE_FWK_MODULE(SiPixelFEDErrorFilter);
