# SiPixelFEDErrorFilter

```
cmsrel CMSSW_13_2_6
cd CMSSW_13_2_6/src/
cmsenv
git clone https://gitlab.cern.ch/CMS-IRB/Pixel/SiPixelTools-SiPixelFEDErrorFilter.git SiPixelTools/SiPixelFEDErrorFilter
scram b -j 4

cd SiPixelTools/SiPixelFEDErrorFilter/python/
cmsRun runFilter_cfg.py
```
